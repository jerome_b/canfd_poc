# CANFD_POC

## Hardware requirements

**Shield Dual CAN and CAN-FD interface:**
https://www.seeedstudio.com/2-Channel-CAN-BUS-FD-Shield-for-Raspberry-Pi-p-4072.html

Tested and OK for Raspberry Pi 3 and 4

**Peak System tools for CAN and CAN-FD:**
https://www.peak-system.com/PCAN-USB.199.0.html?&L=2
https://www.peak-system.com/PCAN-USB-FD.365.0.html?&L=2

## Software requirements

**SSH connection for Raspberry's**  
**Python 3**

For Windows system :

**P-CAN View software, Free and available here:**
https://www.peak-system.com/PCAN-View.242.0.html?&L=2

OR

**Busmaster, Free and open-source and compatible with Peak PCAN-USB :**
https://rbei-etas.github.io/busmaster/


## Shield CAN/CAN-FD installation

**Installation:**

```
git clone https://github.com/seeed-Studio/pi-hats
cd pi-hats/CAN-HAT
sudo ./install.sh 
sudo reboot
```

**Test:**

```
sudo ip link set can0 up type can bitrate 1000000   dbitrate 8000000 restart-ms 1000 berr-reporting on fd on
sudo ip link set can1 up type can bitrate 1000000   dbitrate 8000000 restart-ms 1000 berr-reporting on fd on

sudo ifconfig can0 txqueuelen 65536
sudo ifconfig can1 txqueuelen 65536
```

```
#send data
cangen can0 -mv 
```

```
#dump data
candump can0
```



