import time
import can
import os
import binascii
import struct
import sys
from timeloop import Timeloop
from datetime import timedelta, datetime
from cryptography.hazmat.primitives.ciphers import (Cipher, algorithms, modes)
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding
from cryptography.hazmat.primitives import cmac
from base64 import b64encode

# Variables for CAN and CAN-FD networks
bustype_fd = 'socketcan_native'
channel_fd = 'can0'
bitrate_fd = '1000000'
dbitrate_fd = '2000000'
bustype = 'socketcan'
channel = 'can1'
bitrate = '500000'

def decrypt(key,id,ciphertext):
    """:param key:"""
    """:param id:"""
    """param ciphertext:"""

#    print(f"key value : \t{key.hex()}")
#    print(f"ciphertext    : \t{ciphertext.hex()}")

    iv_recvd = bytes(ciphertext[16:32])
#    print(f"iv_value_recvd    : \t{iv_recvd.hex()}")

    decryptor = Cipher(
        algorithms.AES(key),
        modes.CBC(iv_recvd),
        backend=default_backend()
    ).decryptor()

    now = datetime.now()
#    print("datetime : ", now)
    timestamp = datetime.timestamp(now)
    timestamp = struct.pack(">d",timestamp)
    timestamp = struct.unpack(">d",timestamp)
#    print("timestamp : ",timestamp[0])

    freshness_recvd = bytes(ciphertext[32:40])
#    print(f"freshness_recvd  : \t{freshness_recvd.hex()}")
    timestamp_recvd = struct.unpack(">d",freshness_recvd)
#    print("timestamp_recvd : ",timestamp_recvd[0])
#    print(f"timestamp_recvd_hex    : \t{timestamp_recvd.hex()}")
    datetime_recvd = datetime.fromtimestamp(timestamp_recvd[0])
#    print("datetime_recvd : ", datetime_recvd)
#    print(f"datetime    : \t{datetime_recvd.hex()}")

    if(timestamp_recvd[0]<timestamp[0]-1) or (timestamp_recvd[0]>timestamp[0]+1):
        print("Wrong freshness value detected in frame ",hex(id))
        #sys.exit()
        
    deciphertext = decryptor.update(ciphertext) + decryptor.finalize()
#    print(f"deciphertext : \t{deciphertext.hex()}")
    plaintext = bytes(deciphertext[0:16])
    freshness = bytes(ciphertext[32:48])
    mac = bytes(ciphertext[48:64])
#    print(f"plaintext : \t{plaintext.hex()}")
#    print(f"mac : \t{mac.hex()}")
    c = cmac.CMAC(algorithms.AES(key), backend=default_backend())
    c.update(bytes(ciphertext[0:16])+freshness)
    c.verify(mac)
#    print(f"c : ",c)

    # At this step, Frame OK for redirection
    if(id==0x60D) or (id==0x354):
        producer_gateway(id, plaintext[0:8])
        print("Frame 0x60D OK")


t1 = Timeloop()

@t1.job(interval=timedelta(seconds=0.05))
def consumer():
    bus_fd = can.interface.Bus(channel=channel_fd, bustype=bustype_fd, fd=True)
    for msg in bus_fd:
        decrypt(key,msg.arbitration_id,msg.data)

@t1.job(interval=timedelta(seconds=0.1))
def producer_100():
    wakeup_car = [0x80,0x00,0x00,0x00,0x00,0x00]
    wakeup_dashboard = [0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00]
    msg1 = can.Message(arbitration_id=0x625, data=wakeup_car, is_fd=False, extended_id=False)
    bus.send(msg1)
    msg2 = can.Message(arbitration_id=0x35D, data=wakeup_dashboard, is_fd=False, extended_id=False)
    bus.send(msg2)

def producer_gateway(id, payload):
    """:param id:"""
    """:param payload:"""
    msg3 = can.Message(arbitration_id=id, data=payload, is_fd=False, extended_id=False)
    bus.send(msg3)
    print("Frame sent :", hex(id))


if __name__ == "__main__":

    # Networks configuration
    os.system("sudo ip link set can0 down")
    os.system("sudo ifconfig can0 txqueuelen 65536")
    os.system("sudo ip link set can0 up type can bitrate 1000000 dbitrate 2000000 restart-ms 1000 berr-reporting on fd on")
    os.system("sudo ip link set can1 down")
    os.system("sudo ifconfig can1 txqueuelen 65536")
    os.system("sudo ip link set can1 up type can bitrate 500000")

    # Networks creation
    bus_fd = can.interface.Bus(channel=channel_fd, bustype=bustype_fd, bitrate=bitrate_fd, dbitrate=dbitrate_fd, fd=True)
    bus = can.interface.Bus(channel=channel, bustype=bustype, bitrate=bitrate, fd=False)

    # Key definition
    key = bytes.fromhex('2b7e151628aed2a6abf7158809cf4f3c')

    # Start program
    t1.start(block=True)

