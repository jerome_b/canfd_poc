import time
import can
import os
import binascii
import struct
from timeloop import Timeloop
from datetime import timedelta
from cryptography.hazmat.primitives.ciphers import (Cipher, algorithms, modes)
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding
from cryptography.hazmat.primitives import cmac
from base64 import b64encode
from datetime import datetime

# Variables for CAN-FD network
bustype = 'socketcan_native'
channel = 'can0'
bitrate = '1000000'
dbitrate = '2000000'
speed = []
speed = range(100)
i = 0

def encrypt(key, plaintext):
    """:param key:"""
    """:param plaintext"""

#    print(f"key value : \t{key.hex()}")
#    print(f"key value        : \t{key.hex()}")

    iv = os.urandom(16)
#    print(f"iv_value         : \t{iv.hex()}")

#    print(f"plaintext        : \t{plaintext.hex()}")
    padder = padding.PKCS7(algorithms.AES.block_size).padder()
    plaintext_padded = padder.update(plaintext) + padder.finalize()
#    print(f"plaintext_padded : \t{plaintext_padded.hex()}")

    encryptor = Cipher(
        algorithms.AES(key),
        modes.CBC(iv),
        backend = default_backend()
    ).encryptor()

    ciphertext = encryptor.update(plaintext_padded) + encryptor.finalize()
#    print(f"ciphertext       : \t{ciphertext.hex()}")

    now = datetime.now()
    timestamp = datetime.timestamp(now)
    timestamp = struct.pack(">d",timestamp)
#    print(f"timestamp_hex    : \t{timestamp.hex()}")

    padder = padding.PKCS7(algorithms.AES.block_size).padder()
    freshness_padded = padder.update(timestamp) + padder.finalize()
#    print(f"freshness_value  : \t{freshness_padded.hex()}")

    c = cmac.CMAC(algorithms.AES(key), backend=default_backend())
    c.update(ciphertext+freshness_padded)
    mac = c.finalize()
#    print(f"mac_value        : \t{mac.hex()}")

    return (ciphertext+iv+freshness_padded+mac)

t1 = Timeloop()

@t1.job(interval=timedelta(seconds=0.1))
def producer_100():
    dashboard_signals = bytes.fromhex('0008400000000000')
    msg3 = can.Message(arbitration_id=0x60D, data=encrypt(key,dashboard_signals), is_fd=True, extended_id=False)
    bus_fd.send(msg3)
    print("Frame 0x60D sent")

@t1.job(interval=timedelta(seconds=0.4))
def producer_400():
    global i
    i = (i+1) % 100
#    print(format(speed[i], '02x'))
    speedometer = bytes.fromhex(format(speed[i], '02x')+'00000000000000')
    msg = can.Message(arbitration_id=0x354, data=encrypt(key,speedometer), is_fd=True, extended_id=False)
    bus_fd.send(msg)
    print("Frame 0x354 sent")


if __name__ == "__main__":

    # Network configuration
    os.system("sudo ip link set can0 down")
    os.system("sudo ifconfig can0 txqueuelen 65536")
    os.system("sudo ip link set can0 up type can bitrate 1000000 dbitrate 2000000 restart-ms 1000 berr-reporting on fd on")
    os.system("sudo ifconfig can0 txqueuelen 65536")

    # Networks creation
    bus_fd = can.interface.Bus(channel=channel, bustype=bustype, bitrate=bitrate, dbitrate=dbitrate, fd=True)

    # Key definition
    key = bytes.fromhex('2b7e151628aed2a6abf7158809cf4f3c')

    # Start program
    t1.start(block=True)
