import time
import can
import os
import binascii
import struct
from timeloop import Timeloop
from datetime import timedelta, datetime
from base64 import b64encode

# Variables for CAN network
bustype_0 = 'socketcan_native'
channel_0 = 'can0'
bitrate_0 = '500000'
bustype_1 = 'socketcan'
channel_1 = 'can1'
bitrate_1 = '500000'

i = 0
speed = []
speed = range(100)

def producer_gateway(id, payload):
    """:param id:"""
    """:param payload:"""
    msg3 = can.Message(arbitration_id=id, data=payload, is_fd=False, extended_id=False)
    bus_1.send(msg3)
    print("Frame sent :", hex(id))

def producer_chks_1000(rec_id,rec_data):
    l = len(rec_data)
    idl, idh = divmod(rec_id, 0x100)
    checksum = (idh + idl + l + (sum(rec_data) - rec_data[-1])) & 0xFF
    if(rec_data[-1]==checksum):
        msg_5 = can.Message(arbitration_id=rec_id, data=rec_data, extended_id=False)
        bus_1.send(msg_5)
        print("Frame 0x2E4 with CKS sent")
    else:
        print("Wrong CKS detected !!")

t1 = Timeloop()

@t1.job(interval=timedelta(seconds=0.05))
def gateway_100():
    global msg1_act, msg2_act, msg3_act, msg4_act
    for msg in bus_0:
        if(msg.arbitration_id==0x60D) or (msg.arbitration_id==0x354):
            producer_gateway(msg.arbitration_id, msg.data)
        elif(msg.arbitration_id==0x2E4):
            producer_chks_1000(msg.arbitration_id,msg.data)

@t1.job(interval=timedelta(seconds=0.1))
def producer_100():
    wakeup_car = [0x80,0x00,0x00,0x00,0x00,0x00]
    wakeup_dashboard = [0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00]
    msg1 = can.Message(arbitration_id=0x625, data=wakeup_car, is_fd=False, extended_id=False)
    bus_1.send(msg1)
    msg2 = can.Message(arbitration_id=0x35D, data=wakeup_dashboard, is_fd=False, extended_id=False)
    bus_1.send(msg2)


if __name__ == "__main__":

    # Networks configuration
    os.system("sudo ifconfig can0 txqueuelen 65536")
    os.system("sudo ip link set can0 up type can bitrate 500000")
    os.system("sudo ifconfig can1 txqueuelen 65536")
    os.system("sudo ip link set can1 up type can bitrate 500000")

    # Networks creation
    bus_0 = can.interface.Bus(channel=channel_0, bustype=bustype_0, bitrate=bitrate_0, fd=False)
    bus_1 = can.interface.Bus(channel=channel_1, bustype=bustype_1, bitrate=bitrate_1, fd=False)

    # Start program
    t1.start(block=True)
