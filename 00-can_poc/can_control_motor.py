import time
import can
import os
import binascii
import struct
from threading import Thread
from timeloop import Timeloop
from datetime import timedelta, datetime
from base64 import b64encode

# Variables for CAN network
bustype = 'socketcan'
channel = 'can0'
bitrate = '500000'

t1 = Timeloop()

@t1.job(interval=timedelta(seconds=0.1))
def producer_100():
    dashboard_signals = [0x00,0x08,0x40,0x00,0x00,0x00,0x00,0x00]
    msg3 = can.Message(arbitration_id=0x60D, data=dashboard_signals, is_fd=False, extended_id=False)
    bus.send(msg3)
    print("Frame 0x60D sent")

@t1.job(interval=timedelta(seconds=0.4))
def producer_400():
    speed = []
    speed = range(100)
    i = 0
    i = (i+1) % 100
    #print(format(speed[i], '02x'))
    speedometer = [speed[i],0x00,0x00,0x00,0x00,0x00,0x00,0x00]
    msg = can.Message(arbitration_id=0x354, data=speedometer, extended_id=False)
    bus.send(msg)
    print("Frame 0x354 sent")

@t1.job(interval=timedelta(seconds=1))
def producer_chks_1000():
    payload = [0xF8, 0x00, 0x00, 0x00, 0x00]
    l = len(payload)
    id = 0x02E4
    idl, idh = divmod(id, 0x100)
    checksum = (idh + idl + l + (sum(payload) - payload[-1])) & 0xFF
    payload = [0xF8, 0x00, 0x00, 0x00, checksum]
    msg = can.Message(arbitration_id=id, data=payload, extended_id=False)
    bus.send(msg)
    print("Frame 0x2E4 with CKS sent")
    time.sleep(1)


if __name__ == "__main__":

    # Network configuration
    os.system("sudo ip link set can0 down")
    os.system("sudo ifconfig can0 txqueuelen 65536")
    os.system("sudo ip link set can0 up type can bitrate 500000")

    # Network creation
    bus = can.interface.Bus(channel=channel, bustype=bustype, bitrate=bitrate, fd=False)

    # Start program
    t1.start(block=True)

