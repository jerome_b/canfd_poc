import time
import can
import os
import binascii
from timeloop import Timeloop
from datetime import timedelta
from cryptography.hazmat.primitives.ciphers import (Cipher, algorithms, modes)
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding
from base64 import b64encode


bustype = 'socketcan_native'
channel = 'can0'
bitrate = '500000'

def producer(id):
    speedometer = [0x02,0x10,0xC0,0xFF,0xFF,0xFF,0xFF,0xFF]
    msg = can.Message(arbitration_id=id, data=speedometer, is_fd=False, extended_id=False)
    bus.send(msg)

t1 = Timeloop()

@t1.job(interval=timedelta(seconds=0.1))
def producer_100():
    wakeup_car = [0x80,0x00,0x00,0x00,0x00,0x00]
    wakeup_dashboard = [0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00]
    msg1 = can.Message(arbitration_id=0x625, data=wakeup_car, is_fd=False, extended_id=False)
    bus.send(msg1)
    msg2 = can.Message(arbitration_id=0x35D, data=wakeup_dashboard, is_fd=False, extended_id=False)
    bus.send(msg2)


if __name__ == "__main__":
    
    # Networks configuration
    os.system("sudo ip link set can0 down")
    os.system("sudo ifconfig can0 txqueuelen 65536")
    os.system("sudo ip link set can0 up type can bitrate 500000")

    # Networks creation
    bus = can.interface.Bus(channel=channel, bustype=bustype, bitrate=bitrate, fd=False)
    
    # Start program
    t1.start(block=True)
    
    id = 0
    while id < 2048:
        id = id+1
        producer(id)
        time.sleep(0.01)

